Language Icons Admin
http://drupal.org/project/languageicons_admin
=============================================


DESCRIPTION
-----------
This module shows the current-editing-language icon near translatable form elements in the Drupal administration UI.
It works for both core (per node) translation and entity translation.
It depends on the languageicons module, from which it gets the icons.


REQUIREMENTS
------------
Drupal 7.x
Language Icons 7.x

For a fully enabled multilingual site, the Internationalization (i18n) package
is recommended. See http://drupal.org/project/i18n


INSTALLING
----------
1. To install the module copy the 'languageicons_admin' folder to your
   sites/all/modules directory.

2. Go to admin/build/modules. Enable the module.
Read more about installing modules at http://drupal.org/node/70151


CONFIGURING AND USING
---------------------
1. This module works out of the box, appending the corresponding language icon to the appropriate form elements labels.

There are some configuration options at admin/config/regional/language/icons.
You can place flags before or after the language link or choose to only display
the language flag without the language name (pick "Replace link" under icon
placement to do so). There are some other options so make sure to check it out.


CONTRIBUTING. REPORTING ISSUE. REQUESTING SUPPORT. REQUESTING NEW FEATURE.
--------------------------------------------------------------------------
1. Go to the module issue queue at
   http://drupal.org/project/issues/languageicons_admin?status=All&categories=All
2. Click on CREATE A NEW ISSUE link.
3. Fill out the form.
4. To get a status report on your request go to
   http://drupal.org/project/issues/user


UPGRADING
---------
Read more at http://drupal.org/node/250790

